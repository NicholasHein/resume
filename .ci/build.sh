#!/bin/bash

set -o errexit
set -o pipefail

cat << EOM
================================================================================
******************************* BUILDING PROJECT *******************************
================================================================================

EOM

echo "BOOTSTRAPPING..."
./bootstrap

echo "CONFIGURING..."
./configure

echo "BUILDING..."
make dist

echo "GENERATING CHECKSUM..."
readonly BUILD_TARGET="`find . -maxdepth 1 -name '*.tar.gz' -printf '%P\n'`"
sha256sum "${BUILD_TARGET}" > "${BUILD_TARGET}.sha256sum"

readonly BUILD_VERSION=$(cat ./VERSION)

cat << EOM
================================================================================
Version:____${BUILD_VERSION}
SHA256:_____$(cat ./*.sha256sum | sed -e 's/^\([^\s]\+\)\s.*/\1/')
================================================================================
EOM

# vim:ft=bash
