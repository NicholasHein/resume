#!/bin/bash

set -o errexit
set -o pipefail

readonly PACKAGE_NAME=${PACKAGE_NAME:?package name is unset}
readonly PACKAGE_REGISTRY_URL=${PACKAGE_REGISTRY_URL:?package registry URL is unset}
readonly CI_JOB_TOKEN=${CI_JOB_TOKEN:?CI job token is unset}

# Derive destination URL
readonly PACKAGE_VERSION=$(cat ./VERSION)
readonly PACKAGE_URL=${PACKAGE_REGISTRY_URL}/${PACKAGE_NAME}/${PACKAGE_VERSION}

declare -ri TOTAL_FILES=$#

cat << EOM
================================================================================
**************************** UPLOADING PACKAGE DATA ****************************
================================================================================
Name:___________'${PACKAGE_NAME}'
Version:________${PACKAGE_VERSION}
Upload Files:___${TOTAL_FILES} files
Registry:_______'${PACKAGE_REGISTRY_URL}'
Package URL:____'${PACKAGE_URL}'
================================================================================
EOM

[ 0 -lt $# ] || {
	echo 'ERROR: no files specified!'
	exit 1
}

declare -i upload_count=0

for file_path in "$@"; do
	file_name=$(basename ${file_path})
	file_url=${PACKAGE_URL}/${file_name}

cat << EOM

Uploading: ${file_name}
    src  = '${file_path}'
    dest = '${file_url}'
EOM

	curl \
		--silent \
		--include \
		--verbose \
		--retry 3 \
		--header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
		--upload-file ${file_path} "${file_url}" \
		&& {
		echo 'INFO: upload succeeded'
		upload_count+=1
	} || echo "ERROR: upload failed! (code=$?)"
done

declare -i fail_count=$(($TOTAL_FILES - $upload_count))

cat << EOM

================================================================================
Uploads:____${upload_count} files
Failures:___${fail_count} files
Total:______${TOTAL_FILES} files
================================================================================
EOM

[ 0 -eq $fail_count ] || exit 1

# vim:ft=bash
