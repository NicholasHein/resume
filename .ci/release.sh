#!/bin/bash

set -o errexit
set -o pipefail

readonly PACKAGE_NAME=${PACKAGE_NAME:?package name is unset}
readonly PACKAGE_REGISTRY_URL=${PACKAGE_REGISTRY_URL:?package registry URL is unset}
readonly CI_COMMIT_TAG=${CI_COMMIT_TAG:?CI commit tag is unset}
readonly CI_JOB_TOKEN=${CI_JOB_TOKEN:?CI job token is unset}

readonly RELEASE_NAME="v${CI_COMMIT_TAG}"
declare -ri TOTAL_ASSETS=$#

# Derive asset link base URL
readonly PACKAGE_VERSION=$(cat ./VERSION)
readonly PACKAGE_URL=${PACKAGE_REGISTRY_URL}/${PACKAGE_NAME}/${PACKAGE_VERSION}

cat << EOM
================================================================================
*************************** CREATING PACKAGE RELEASE ***************************
================================================================================
Release:________'${RELEASE_NAME}'
Commit Tag:_____'${CI_COMMIT_TAG}'
Asset Links:____${TOTAL_ASSETS} links
Package:________'${PACKAGE_NAME}'
Version:________${PACKAGE_VERSION}
Registry:_______'${PACKAGE_REGISTRY_URL}'
Package URL:____'${PACKAGE_URL}'
================================================================================

EOM

if [ 0 -ne $TOTAL_ASSETS ]; then
	echo "Adding asset links:"
else
	echo "No asset links"
fi

declare -a asset_link_args=()
for file_path in "$@"; do
	file_name=$(basename ${file_path})
	file_url=${PACKAGE_URL}/${file_name}
	case "$file_name" in
		*.tar.gz)
			file_type='package'
			;;
		*)
			file_type='other'
			;;
	esac

	echo "  - ${file_name} (${file_type}): ${file_url}"
	asset_link_args+=(\
		'--assets-link' \
		"{\"name\":\"${file_name}\",\"filepath\":\"/${file_name}\",\"url\":\"${file_url}\",\"link_type\":\"${file_type}\"}" \
	)
done

declare -i exit_code=0
release-cli create \
	--name ${RELEASE_NAME} \
	--tag-name ${CI_COMMIT_TAG} \
	${asset_link_args[@]} \
	&& echo "INFO: created the release successfully!" \
	|| {
	exit_code=$?
	echo "ERROR: failed to create release! (code=${exit_code})"
}

[ 0 -eq $exit_code ] || exit $exit_code

# vim:ft=bash
