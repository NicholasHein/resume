<div align="center">
  <img
    alt="Nicholas Hein's Resume"
    title="Avatar of Nicholas Hein's"
    src="https://www.paradoxdev.com/images/avataaars.svg"
  />
</div>

## Hello!

... and welcome! Thanks for taking some time to checking out my resume's source
code, and thank you even more for considering me as a candidate!

I hope you like what you see in both regards.  If you have any questions,
concerns, or suggestions, feel free to send me an email at
[nhein@paradoxdev.com](mailto:nhein@paradoxdev.com),
or give me a call at the number listed on my resume.

Also, please feel free to check out my website,
[paradoxdev.com](//paradoxdev.com),
to learn more about me.

__NOTE:__ if you're viewing this from one of the distribution tarballs, you
might prefer to check out this project's
[GitLab repository](//gitlab.com/NicholasHein/resume).

## Contents

[TOC]

## How do I build it?

There are multiple ways to build this project depending on your intentions.
Most likely, if you're here, I gave you a pre-built version of my resume.  But,
if you're interested in trying it yourself, want to fork this project to make
your own resume, or want to install it to your computer as an update-able
package (yes you can do that if you want): I have options for you!

It's licensed as GPLv3, so you can do whatever you want with it -- unless you
redistribute a derivative work of it yourself.  In that case, it you would also
have to license it as GPLv3 and make all modifications available to the free
software community!  But no matter what, I'd encourage you to clone it, build
it, and see it for yourself!

To do this, this project supports:

- Building the resume [directly from a clone](#from-source) of this repository
- Building the resume [from the tarball](#from-a-release) included in releases
- Creating your own [distribution tarball](#for-distribution)
- [Packaging a distribution](#as-a-package) for Archlinux's `pacman`
- ... and possibly [without Linux](#without-linux)

### Requirements

__ArchLinux__

- `coreutils`: for essential basic commands (you probably already have this)
- `autoconf`: to configure the build chain
- `automake`: to generate build scripts
- `make`: to build the project
- `texlive-bin`: to compile the resume from LaTeX
- `git` (optional): for cloning the repo or running the boostrap script
- `xdg-user-dirs` (optional): to determine your home directory

__Other Distro/OS__

You should be able to build this project on any system that has infrastructure
for building the *'pdfTeX'* flavor of LaTeX.  That's really all you need.
However, if you plan on using the build chain, you'll need the full GNU
AutoTools suite.

Whether you use a package manager, installer, or build these tools from source
(good luck), the binaries you'll need are the same.  If you already have the
basic stuff (like `sh`, `awk`, `install`, etc.), you'll also want to get these:

- [Autoconf (GNU)](//www.gnu.org/software/autoconf)
- [Automake (GNU)](//www.gnu.org/software/automake)
- [Make (GNU)](//www.gnu.org/software/make)
- [pdfLatex (Tex Live)](//tug.org/texlive)

And, if you want to try the bootstrap script:

- [Git (Git Community)](//git-scm.com/downloads)
- [xdg-user-dirs (freedesktop.org)](//www.freedesktop.org/wiki/Software/xdg-user-dirs) - optional

### From source...

In order to build this resume directly from it's source, you have to...

0. First clone it from GitLab:

If you have a GitLab account (using SSH):

```bash
  $ git clone git@gitlab.com:NicholasHein/resume.git nicholas-hein-resume
```

Or, if you don't have a GitLab account (using HTTPS):

```bash
  $ git clone https://gitlab.com/NicholasHein/resume.git nicholas-hein-resume
```

__Using the bootstrap script:__

1. If you want to make your life a bit easier by using the bootstrap script,
   you first need to install the required dependencies. (if you want to make
   this process easier on yourself, I'd suggest installing `xdg-user-dirs`)
2. Run the bootstrap script:

```bash
  $ ./bootstrap
  $ # ... or, if you want to choose the install destination yourself:
  $ ./bootstrap --with-outputdir=<your_output_dir>
  $ # ... or, if you want to pass any other options to './configure':
  $ ./bootstrap <options>
```

3. Run `make`
4. If successful, you should find `nicholas_hein-resume.pdf` in the `/src`
   directory!

__Building manually:__

1. If you'd rather have fine-grained control over how this project is built
   (without using the bootstrap script), you only really have to install the
   required dependencies.
2. Create the `/VERSION` file.  The preferred way to do this is:

```bash
  $ git describe --always --dirty --abbrev >VERSION
```

3. Configure the build-chain with `autoreconf`:

```bash
  $ autoreconf --install
```

4. Run the configure script generated by the previous step:

```bash
  $ ./configure
  $ # ... or, if you want to choose the install destination yourself:
  $ ./configure --with-outputdir=<your_output_dir>
```

5. Run `make`
6. If successful, you should find `nicholas_hein-resume.pdf` in the `/src`
   directory!

#### For distribution...

If you plan on distributing a build of this project to others, I'm guessing
it's probably a good idea if ya' test it before ya' ship it!

0. Follow the steps above to build the project from source.  It's always a good
   idea to be sure you can build it first before you try to do anything fancy
   with it.
1. Run `make dist`
2. Voila!  If successful, you should find
   `nicholas-hein-resume-<version>.tar.gz` in the project's root directory.
   Yeah, it's that easy!

#### As a package...

This part is a bit more tricky.  Basically all ArchLinux packages keep their
`PKGBUILD` file separate from their actual source.  It's just package
distribution 101.  But, that's not fun!  Let's make this harder on ourselves!

0. First, we want to nuke all the build artifacts we've made previously.  This
   is because the build-chain we generated embedded a lot of information about
   our working version number in there.  To build the package, we'd either need
   to start from a clean-cut and versioned commit, or we need to commit our
    changes and create a new one!  Either way it might be a good idea to clean
    things up a bit, just in case, by using:

```bash
  $ ./bootstrap clean
  $ # ... which uses this under-the-hood:
  $ git clean -dX --force
```

1. Next up, we want to create a distribution we can package.  It's going to be
   *way* easier on you to just use the `./bootstrap` script.  This process
   involves a lot of manual steps which have to be done in just the right
   order, and -- if you wanted to do it that way -- you may just want to look
   at the code in the script for reference.  However, I included the basic
   sequence along with the easy way below:
   1. Come up with a new version number.
   2. Bootstrap the project using this version number.
   3. Build the distribution tarball.
   4. Change the version number in the `PKGBUILD` file.
   5. Generate the MD5 checksum of this tarball.
   6. Replace the MD5 checksum in the `PKGBUILD` file.
   7. Commit these new changes.
   8. Create an annotated tag that points to that commit.

```bash
  $ # The easy-way-out:
  $ ./bootstrap dist ${YOUR_NEW_VERSION_NUMBER}
  $ # This command guides you through that process and only asks you for
  $ # interaction in places it needs to.  You will only be prompted for:
  $ #   1. The commit message
  $ #   2. The tag message
```

2. Lastly, we generate the ArchLinux `pacman` package using:

```bash
  $ makepkg -s
```

3. If successful, you should find
   `nicholas-hein-resume-<version>-any.pkg.tar.zst` in the project's root
   directory which you can distribute to all your friends!  If they want to
   install it, they just have to use:

```bash
  $ sudo pacman -U nicholas-hein-resume-<version>-any.pkg.tar.zst
```

Easy, right?

### From a release...

You probably thought I was going to ramble on about yet another way to build
this project yet again, right?  Well, I have good news for you!...  I'll only
sort-of do it.

This time, things are a bit easier, because we did all the dirty work above in
the section about [building from source](#from-source).  The only hick-up here
is that first we need to extract the release tarball first:

```bash
  $ # You might want to create an output directory first:
  $ mkdir out
  $ # Then, in typical 'tar' humor... Xtract Zee ******* Files!!!
  $ tar xzvf ./nicholas-hein-resume-${VERSION}.tar.gz -C ./out/
```

Then, we just need to build it from source, but this time _without_ our helpful
friend, the `./bootstrap` script:

```bash
  $ cd out
  $ ./configure --with-outputdir=<your_output_dir>
  $ make
  $ make install
```

### Without Linux...

How should I know?  I haven't used Windows or Mac OS in years!

I'm totally kidding, but I seriously haven't tried this.  But I would suggest
keeping it simple and putting it in a good LaTeX editor like:

- [Overleaf](//www.overleaf.com/)
- [TeXstudio](//www.texstudio.org/)
- [TeXmaker](//www.xm1math.net/texmaker/)

## Why so much code!?

I'm glad you asked! And I'm happy that I was able to pique your curiosity
enough to dig deeper into some of my projects. This repository may seem a bit
excessive for such a simple LaTeX document, but please let me explain!  My goal
for this project was to try to showcase just a choice few of my abilities to
you by applying them to a basic project that is relevant to you. I did this in
the hopes that you'll be able to get a better picture of what skills I could
bring to your team.

In order to accomplish this goal, I had to ask myself:

> How do I even go about showing off my abilities with a project so simple that
> it's basically just a one-page paper?

My solution: (_irony intended_)

> I know!  I can... *over-complicate it!* \
> \[thunder strikes in the distance\] \
> MUAH HA HA HAAAA!

All jokes aside, though, I do pride myself on my ability to find _simple_ and
_elegant_ solutions to complex problems, but I wanted to be sure that you could
easily preview some of my work without needing to browse through all of my
public repositories.  **Your time is valuable**, and I wanted to be sure you could
get the best picture of me while using as little of it as possible.

## What does the code do?

There are four main components in this project:

1. **The Resume:** is compiled from a single LaTeX
   [source file](file://src/resume.tex) using [TexLive](//tug.org/texlive/).
   You can see more about how this is done [here](#how-do-i-build-it).
2. **Autotools scripts:** generate the build-chain using
   [GNU Autoconf](//www.gnu.org/software/autoconf/)
   and [build](#from-source) the resume from its source using
   [GNU Automake](//www.gnu.org/software/automake/).
3. **Archlinux Package:**  I use [Archlinux](//archlinux.org/) by the way,
   so I had to make it especially easy for any other Arch fans out there to
   install my resume to their home directory, of course.  The
   [`PKGBUILD`](file://PKGBUILD) file makes this happen using native Archlinux
   [package distribution](#as-a-package) tools.
4. **Bootstrap script:** is a [shell script](file://bootstrap) that makes
   development easier (and prettier) by abusing ANSI escape codes codes while
   also automating a few common tasks needed for testing and distribution like:
  - `./bootstrap [ARGS]`: configures the build chain using Autoreconf, passing
    `ARGS` to the generated [`configure script`](file://configure)
  - `./bootstrap clean`: cleans up all the garbage generated while bootstrapping
    (it uses `git clean` under the hood)
  - `./bootstrap dist <VER>`: creates a new distribution of this project by...
    * Generating a good ol' distribution tarball for the supplied version `VER`
    * Substituting appropriate parameters into [`PKGBUILD`](file://PKGBUILD)
      such as the new version number and MD5 checksum
    * Committing those changes and tagging that commit with the given version

### What _doesn't_ the code do?

There are many things this project doesn't do that I wish it could.  But, in
order to keep this project simple <!-- ;) --> and easy to peruse at your
leisure, I couldn't really show you much of what I can do.  If it was feasible,
I would have liked to show you my skills a bit better:

1. This project centers around a LaTeX document, and although I believe
   documentation is essential, I am primarily an embedded systems engineer.
   So, I would have preferred to have written my resume in C if that was
   possible. Wait... Now that's an idea!  I'll put it in the project
   [road-map](#road-map).  But, really, I have experience implementing code at
   all levels from [firmware](//gitlab.com/NicholasHein/tripwire-trap)
   to GUI apps including...
   - Kernel drivers
   - Low-level libraries
   - System-level daemons
   - CLI programs
   - GUI apps
   - Web app front-ends/back-ends

2. I made and distributed this using Linux, Autotools, scripts, Git, GitLab and
   my knowledge of software design.  However, this doesn't even touch the tip of
   the iceberg, so to speak, of my experience in software/computer engineering.
   If it did, it would also involve technologies like...
   - CMake
   - Makefiles
   - GCC/Clang
   - Various CI/CD suites
   - [Gerrit](//www.gerritcodereview.com/)
   - And even [Yocto](//www.yoctoproject.org/)!
     Though, that may be a tad excessive for just this resume.

3. The scope of this project is extremely limited, but making software and
   systems that are simple, verifiable, and maintainable is not only my job;
   it's my hobby as well!  If this project were to completely illustrate the
   scope of projects I've done, it would have to touch at least the following
   domains...
   - [Real-time systems](//gitlab.com/NicholasHein/fishla)
   - [Network protocols](//paradoxdev.com/linux-kernel-network-stack.pdf)
   - HDMI with expertise in
     [CEC (consumer electronics control)](//en.wikipedia.org/w/index.php?title=Consumer_Electronics_Control&oldid=1078544303),
     [EDID (Extended Display Information Data)](//en.wikipedia.org/w/index.php?title=Extended_Display_Identification_Data&oldid=1084803601),
     and related specifications

## Road-map

- Figure out some way to rewrite this resume in Rust or C (if possible, of
  course).  Maybe I could settle for doing something like one of my favorite
  open-source projects: the incredible
  [business card that runs Linux](//www.thirtythreeforty.net/posts/2019/12/my-business-card-runs-linux/)
  by [George Hilliard](//www.thirtythreeforty.net/):!

## Special Notes

There are a few things I wanted to be sure to mention:

- I made this project not only to create a professional resume for myself but
  also to learn about GNU Autotools.  I always love learning new things,
  especially things that make me more qualified and knowledgeable about
  computers. Since age 12, I've always loved programming, and I thrive on
  teaching myself new things.  If you decide that I would be a good fit for a
  position on your team, I can promise that I will bring that same attitude and
  drive when working with you.
- I got started with this resume by forking the LaTeX file from the "classic"
  template written by [Jan Küster](//github.com/jankapunkt).  Their
  project, ["latexcv"](//github.com/jankapunkt/latexcv), was extremely
  helpful when I was first writing my own for this project.  In the spirit of
  open-source, I was sure to include their original MIT license in
  [that file](file://src/resume.tex).

## [License](file://LICENSE)

Nicholas Hein's Resume
Copyright (C) 2022  Nicholas R. Hein

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
